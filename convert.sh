#!/bin/bash

docker="$(which docker)"
script="$0"
basename="$(dirname $0)"

files="$(find . -name 'temp.pdf')"
# echo $files | sed -e 's/\.//g'
#find . -name 'temp.pdf'

list(){
    for file in "$1"/* 
    do 
        if [ -f "$file" ] 
        then
            echo "$file"
        fi

        if [ -d "$file" ] 
        then
            list "$file"
        fi

    done
}
list "$basename/temp/output"

#$docker