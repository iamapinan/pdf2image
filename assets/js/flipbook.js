$(function () {
    var win = $(this); //this = window

    if (win.width() >= 1000) {
        $(".viewport").height(win.height() - 35);
        $("#flipbook").turn({
            autoCenter: true,
            turnCorners: "bl,br",
            peel: "l,r",
            elevation: 100,
            gradients: !$.isTouch,
            display: 'double',
            width: 1000,
            height: "auto"
        }, 'zoom');

        // Zoom
        $('.viewport').zoom({
            flipbook: $('#flipbook'),
            max: function () {

                return win.width() / $('#flipbook').width();

            },
            when: {
                tap: function (event) {

                    if ($(this).zoom('value') == 1) {
                        $('#flipbook').
                            removeClass('animated').
                            addClass('zoom-in');
                        $(this).zoom('zoomIn', event);
                    } else {
                        $(this).zoom('zoomOut');
                    }
                },

                resize: function (event, scale, page, pageElement) {

                    if (scale == 1)
                        loadSmallPage(page, pageElement);
                    else
                        loadLargePage(page, pageElement);

                },

                zoomIn: function () {

                    $('#flipbook').addClass('zoom-in');

                    if (!window.escTip && !$.isTouch) {
                        escTip = true;

                        $('<div />', { 'class': 'esc' }).
                            html('<div>Press ESC to exit</div>').
                            appendTo($('body')).
                            delay(2000).
                            animate({ opacity: 0 }, 500, function () {
                                $(this).remove();
                            });
                    }
                },

                zoomOut: function () {

                    $('.esc').hide();
                    $('.made').fadeIn();

                    setTimeout(function () {
                        $('#flipbook').addClass('animated').removeClass('zoom-in');
                    }, 0);

                },

                swipeLeft: function () {

                    $('#flipbook').turn('next');

                },

                swipeRight: function () {

                    $('#flipbook').turn('previous');

                }
            }
        });
    }
    else {
        $("#flipbook").turn({
            autoCenter: true,
            turnCorners: "bl,br",
            peel: "l,r",
            elevation: 100,
            gradients: !$.isTouch,
            display: 'single',
            width: win.width(),
            height: win.height() - 45
        }, 'zoom').css('margin-top', '5px');
    }


    $(window).bind('keydown', function (e) {
        if (e.keyCode == 37)
            $('#flipbook').turn('previous');
        else if (e.keyCode == 39)
            $('#flipbook').turn('next');
    });

    $("#pageFld").val($("#flipbook").turn("page"));

    $("#flipbook").bind("turned", function (event, page, view) {
        $("#pageFld").val(page);
    });

    $("#pageFld").change(function () {
        $("#flipbook").turn("page", $(this).val());
    });


    $("#prevBtn").click(function () {
        $("#flipbook").turn("previous");
    });

    $("#nextBtn").click(function () {
        $("#flipbook").turn("next");
    });
});

// Load large page

function loadLargePage(page, pageElement) {

    var img = $('<img />');

    img.load(function () {

        var prevImg = pageElement.find('img');
        $(this).css({ width: '100%', height: '100%' });
        $(this).appendTo(pageElement);
        prevImg.remove();

    });

    // Loadnew page

    img.attr('src', 'pages/' + page + '-large.jpg');
}

// Load small page

function loadSmallPage(page, pageElement) {

    var img = pageElement.find('img');

    img.css({ width: '100%', height: '100%' });

    img.unbind('load');
    // Loadnew page

    img.attr('src', 'pages/' + page + '.jpg');
}