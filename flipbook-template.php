<?php header('X-Frame-Options: SAMEORIGIN'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Flipbook by iOTech</title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <link rel="stylesheet" href="{domain}/assets/css/flipbook.css">
    <!--{pageheight}-->
</head>

<body>
    <div class="viewport">
        <div id="flipbook">
            {page}
        </div>
    </div>
    <div class="menu-bottom">
        <button id="prevBtn">Previous</button>
        <input type="number" id="pageFld" min=1 readonly/>
        <button id="nextBtn">Next</button>
    </div>
    <script src="{domain}/assets/js/jquery.min.js"></script>
    <script src="{domain}/assets/js/turn.min.js"></script>
    <script src="{domain}/assets/lib/zoom.min.js"></script>
    <script src="{domain}/assets/js/flipbook.js"></script>
</body>
</html>