<?php
date_default_timezone_set('Asia/Bangkok');
/**
 * Recursively removes a folder along with all its files and directories
 * 
 * @param String $path 
 */
function rrmdir($path, $origin = null) {
    if($origin == null) {
        $origin  = $path;
    }
    $objects = scandir($path);
    
    foreach ($objects as $object) { 
        if ($object != "." && $object != ".." && filemtime($path."/".$object) < time() - 3600) { 

                echo $path."/".$object ." Deleted <br>";
                if (is_dir($path."/".$object)) {
                    echo $object;
                    $db = new SQLite3(__DIR__ . '/db.sqlite');
                    $db->query("delete from process where fkey = '$object'");
              
                    rrmdir($path."/".$object, $origin);
                }
                else {
                    unlink($path."/".$object); 
                }
            }
            
        }
        if( $path != $origin ) 
            rmdir($path);

        // $db = new SQLite3(__DIR__ . '/db.sqlite');
        // $db->query("delete from process where fkey = ");
        
}
rrmdir(__DIR__ . '/temp/output');