<?php header('X-Frame-Options: SAMEORIGIN'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Viewer by iOTech</title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <link rel="stylesheet" href="{domain}/assets/css/pdf.css">
</head>
<body>
    <div class="viewport">
        {page}
    </div>
</body>
</html>