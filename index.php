<?php
/**
 * PDF to image converter.
 * Required imagemagick library for PHP. Allow to convert file using API supported by GET, POST method.
 * Licensed by IOTech Enterprise Co.,Ltd.
 * 
 * @author Apinan Woratrakun <apinan@iotech.co.th>
 * @param 
 * - url    = Url of pdf
 * - format = Convert file type
 * @license GNU 2.0
 * @version 1.0.0
 */

// Set JSON header
header('Content-type: application/json');
include_once('vendor/autoload.php');

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();
$domain = getenv('ServerURL');

if(!file_exists(__DIR__ . 'db.sqlite')) {
    // Prepare database.
    $db = new SQLite3(__DIR__ . '/db.sqlite');
    $create_table_cmd = 'CREATE TABLE IF NOT EXISTS process (
                            _id   INTEGER PRIMARY KEY,
                            status INTEGER (5) NOT NULL,
                            fkey    VARCHAR (100) NOT NULL,
                            size    INTEGER (3) NOT NULL
                        )';
    $db->query($create_table_cmd);
}

if(isset($_REQUEST['url'])):
    // Prepare
    $url = $_REQUEST['url'];
    $id = uniqid();
    $tmp_name = 'temp.pdf';
    $folder =  __DIR__ . "/temp/output/".$id;

    // Create folder
    if(!is_dir($folder)) {
        mkdir($folder, 0755);
    }

    // Assign file path
    $file_path = $folder . '/' . $tmp_name;

    list($status) = get_headers($url);
    if (strpos($status, '200') !== FALSE) {

        // Download pdf from url.
        file_put_contents( $file_path, file_get_contents($url) );

        // Get pdf page count.
        $im = new imagick($file_path);
        $page_number = $im->getNumberImages();
        $im->clear(); 
        $im->destroy();

        $res = [
            "status"    => "processing",
            "create_at" => time(),
            "data_id"   => $id,
            "size"      => $page_number
        ];
    } else {
        http_response_code(400);
        $res = [
            "error" => "Url can't be access."
        ];
    }

    // Insert new order
    $db->query("INSERT INTO process VALUES(NULL, 1, '$id', $page_number)");
    echo json_encode($res);
    exit;
endif;


if(isset($_REQUEST['id'])):
    // Select
    $id = $_REQUEST['id'];
    $data = $db->query("SELECT * FROM process WHERE fkey='$id'")->fetchArray(SQLITE3_ASSOC);

    if(!empty($data)):

        if($data['status'] == 1):
            $res['status'] = 'processing';
        elseif($data['status'] == 2):
            $res['status'] = 'waiting';
        elseif($data['status'] == 3):
            $res = [
                "status" => "success",
                "data" => json_decode( file_get_contents(__DIR__ . "/temp/output/" . $data['fkey'] ."/response.json") )
            ];
        endif;

        echo json_encode($res);
    else:
        http_response_code(404);
        echo json_encode(['error' => "Process $id not exists"]);
    endif;

    exit;
endif;


if($argv[1] == 'convert'):

    $data = $db->query("SELECT * FROM process WHERE status = 1 ORDER BY _id ASC LIMIT 0,1")->fetchArray(SQLITE3_ASSOC);
    if(!empty($data)):
        // Initial variables.
        $folder =  __DIR__ . "/temp/output/" . $data['fkey'];

        // Run convert
        exec("docker run -it -d --rm \
        -v $folder/temp.pdf:/var/workdir/input.pdf \
        -v $folder/:/var/workdir/output/ \
        kolyadin/pdf2img -q -r 180 -jpeg -aa yes -aaVector yes -freetype yes input.pdf output/page
        2>&1");

        $db->query("UPDATE process SET status=2 WHERE fkey='".$data['fkey']."' ");
    endif;
    exit;
endif;

if($argv[1] == 'end'):
    $data = $db->query("SELECT * FROM process WHERE status = 2 ORDER BY _id ASC LIMIT 0,1")->fetchArray(SQLITE3_ASSOC);

    $folder =  __DIR__ . "/temp/output/" . $data['fkey'];
    $temp =  $folder . "/temp.pdf";
    $page_number = $data['size'];
    $flipbook_text = "";
    $pdf_text = "";

    $read = scandir($folder);
    $fileCount = count($read)-2;

    if ($page_number != 0 && $fileCount >= $page_number) {

        for ($i = 01; $i < $page_number; $i++) {
            // formatting number
            $page = str_pad($i, 2, 0, STR_PAD_LEFT);

            // html output
            $flipbook_text  .= "<div class='pgs><img src='page-$page.jpg'></div>\n";
            $pdf_text       .= "<div><img src='page-$page.jpg'></div>\n";

            // Json response
            $res['images'][$i] = $domain . "/temp/output/".$data['fkey']."/page-$page.jpg";
        }

        // Flipbook
        $flipbook = file_get_contents(__DIR__ . "/flipbook-template.php");
        $flipbook = str_replace("{page}", $flipbook_text, $flipbook);
        $flipbook = str_replace("{domain}", $domain, $flipbook);

        // PDF
        $pdf = file_get_contents(__DIR__ . "/pdf.php");
        $pdf = str_replace("{page}", $pdf_text, $pdf);
        $pdf = str_replace("{domain}", $domain, $pdf);
        
        // Save output file.
        file_put_contents($folder . "/index.php", $flipbook);
        file_put_contents($folder . "/pdf.php", $pdf);

        // Success response
        $res['pageCount'] = $data['size'];
        $res['id'] = $data['fkey'];
        $res['pdf'] = $domain . "/temp/output/".$data['fkey']."/pdf.php";
        $res['flipbook'] = $domain . "/temp/output/".$data['fkey']."/index.php";
        $res['zip'] = $domain . "/temp/output/".$data['fkey']."/".$data['fkey']."_data.zip";
        
        $writeResponse = file_put_contents( $folder ."/response.json", json_encode($res));
        if($writeResponse) {
            exec("rm $temp && \
            cd $(pwd)/temp/output && \
            zip -r ".$data['fkey']."_data.zip ".$data['fkey']."/ && \
            mv ".$data['fkey']."_data.zip ".$data['fkey']."/
            ", $op);
            $db->query("UPDATE process SET status=3 WHERE fkey='".$data['fkey']."' ");
        }
    }
    exit;
endif;